const readline = require('readline');
const fs = require('fs-extra');

module.exports = async function(filePath, trim = true) {
	return new Promise(resolve => {
		const lines = []

		const rl = readline.createInterface({
			input: fs.createReadStream(filePath),
			output: process.stdout
		})

		rl.on('line', line => lines.push(trim ? line.trim() : line))

		rl.on('close', line => resolve(lines))
	})
}