const puppeteer = require('puppeteer')
const fs = require('fs-extra')
const clipboardy = require('clipboardy')
const getLines = require('./getLines.js')

Object.defineProperty(Array.prototype, 'chunk', {
	value: n =>
		Array(Math.ceil(this.length / n)).fill().map((_, i) => this.slice(i * n, i * n + n))
})

const partitionArray = (array, size) => array.map( (e,i) => (i % size === 0) ? array.slice(i, i + size) : null ) .filter( (e) => e )

let browser

(async () => {
	try {
		browser = await puppeteer.launch({headless: false, ignoreHTTPSErrors: true})

		const urlChunks = partitionArray(Array.from(await getLines('urls.txt')), 5)
		console.log({urlChunks})

		for (let chunkIndex = 0; chunkIndex < urlChunks.length; chunkIndex++) {
			await loop(urlChunks[chunkIndex])
		}
	}
	catch (crawler) {
		console.error({crawler})
	}
	finally {
		await browser.close()
	}

})()

async function loop(urls) {
	let page, clipboardText

	for (const [urlIndex, url] of urls.entries()) {
		try {
			console.log(`looping url: ${url}\n`)

			page = await browser.newPage()

			await page.setRequestInterception(true)

			page.on('request', request =>
				request.resourceType() === 'image' ? request.abort() : request.continue())

			await page.goto(url, {waitUntil: 'domcontentloaded', timeout: 60000})

			await interaction()
		}
		catch (loopError) {
			console.error({loopError})
		}
		finally {
			if (page && urlIndex !== (urls.length - 1)) {
				await page.close()
			}
		}
	}

	async function interaction() {
		await keyboardCombo('KeyA')
		await keyboardCombo('KeyC')

		clipboardText = await clipboardy.read()

		if (clipboardText.includes('Checking your browser')) {
			await page.waitFor(5000)

			await keyboardCombo('KeyA')
			await keyboardCombo('KeyC')
			clipboardText = await clipboardy.read()
		}

		await write()

		async function keyboardCombo(press) {
			await page.keyboard.down('Control')
			await page.keyboard.press(press)
			await page.keyboard.up('Control')
			console.log(`Pressed Ctrl-${press.substr(press.length - 1)}`)
		}

		async function write() {
			try {
				await fs.writeFile('./output.txt', clipboardText, {flag: 'a'})
				console.log('success!')
			}
			catch ({writeLog}) {
				console.error({writeLog})
			}
		}
	}
}